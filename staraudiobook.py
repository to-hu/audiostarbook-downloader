#!/usr/bin/env python3
import requests
import lxml.html as lh
import re
import os
import sys

SERIES_URL = "https://staraudiobook.com/series/harry-potter"

def get_as_tree(url):
    r = requests.get(url)
    return lh.document_fromstring(r.text)

def get_max_page_number_series(url):
    tree = get_as_tree(url)
    pages = [x.text for x in tree.find_class("page-numbers") if x.tag == 'a']
    return max([int(x)for x in pages if re.match(r"\d+", x)])

def get_book_urls(series_url):
    count_pages = get_max_page_number_series(series_url)
    acc = []
    for page in range(1, count_pages + 1):
        tree = get_as_tree(SERIES_URL + "/page/" + f'{page}')
        acc = acc + [x.getchildren()[0].attrib["href"] for x in tree.find_class("entry-title post-title")]
    return acc

def get_audio_urls(url):
    def get_pages(result, nr):
        tree = get_as_tree(url + "/" + str(nr))
        audios_children = [x.getchildren() for x in tree.find_class("wp-audio-shortcode")]
        audio_urls = [x[1].attrib["href"] for x in audios_children]
        if len(audio_urls) == 0:
            return []
        else:
            return audio_urls + get_pages(result + audio_urls, nr + 1)

    return get_pages([], 1)


def get_filename_from_audio_url(url):
    return [x.replace('%', '_') for x in url.split("/")][-1]


def get_foldername_from_book_url(url):
    return [x for x in url.split("/") if x != ''][-1]


def download(url, dest):
    r = requests.get(url, allow_redirects=True)
    with open(dest, 'wb') as f:
        f.write(r.content)


def main(series_url):
    for book in get_book_urls(series_url):
        foldername = os.path.join(os.getcwd(), get_foldername_from_book_url(book))
        os.mkdir(foldername)
        for audio in get_audio_urls(book):
            filename = os.path.join(foldername, get_filename_from_audio_url(audio))
            download(audio, filename)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        main(SERIES_URL)
    elif len(sys.argv) == 2:
        main(sys.argv[-1])
    else:
        sys.exit(1)
